# ConfigCollector

A Ruby library for collecting configuration information.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'config_collector'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install config_collector

## Usage

Approach #1:

```ruby
ConfigCollector.call(:one, :two, :three) do |cc|
  cc.one    = "one"
  cc.two    = 2
  cc.three  = 3.0
end #=> ["one", 2, 3.0]
```

Approach #2:

```ruby
class ThingThatNeedsAConfig
  attr_accessor :one, :two, :three

  CONFIG_COLLECTOR = ConfigCollector.new(:one, :two, :three)

  def initialize(&block)
    @one, @two, @three = CONFIG_COLLECTOR.call(&block)
  end
end

thing =
  ThingThatNeedsAConfig.new do |config|
    config.one    = "one"
    config.two    = 2
    config.three  = 3.0
  end

thing.one   #=> "one"
thing.two   #=> 2
thing.three #=> 3.0
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/config_collector.

