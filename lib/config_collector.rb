require "config_collector/version"

class ConfigCollector
  def initialize(*keys)
    @keys = keys
  end

  def call(&block)
    struct = Struct.new(*@keys).new

    block.call(struct)

    struct.values
  end

  def self.call(*keys, &block)
    new(*keys).call(&block)
  end
end

