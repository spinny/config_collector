require 'spec_helper'

describe ConfigCollector do
  it 'works' do
    ret =
      ConfigCollector.call(:one, :two, :three) do |cc|
        cc.one    = "one"
        cc.two    = 2
        cc.three  = 3.0
      end

    expect(ret).to eq(["one", 2, 3.0])
  end
end
